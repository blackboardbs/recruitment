<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\App;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Client;

class ClientTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    protected $client;
    protected function setUp(): void
    {
        parent::setUp();
        $client = factory('App\Client')->create();

        $this->client = $client;
    }

    /** @test */
    public function is_an_instance_of_client()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(Client::class, $this->client);
    }
}
