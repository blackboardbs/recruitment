<?php

namespace Tests\Feature;

use App\Address;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Candidate;
use Carbon\Carbon;

class CandidateTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    protected $candidate;

    protected function setUp(): void
    {
        parent::setUp();
        $this->candidate = factory('App\Candidate')->create();
    }

    /** @test */
    public function does_candidate_class_exist(){
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(Candidate::class, $this->candidate);
    }


    /** @test */
    public function is_an_instance_of_carbon(){
        $this->assertInstanceOf(Carbon::class, $this->candidate->date_of_birth);
    }

    /** @test */
    public function candidate_can_be_created()
    {
        $this->withoutExceptionHandling();
        factory(Address::class)->create();
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'user' => $user->toArray(),
            'bio' => $this->faker->text(300),
            'preference_description' => $this->faker->text(350),
            'title' => $this->faker->title,
            'preferred_address_id' => $this->faker->numberBetween(1,10),
            'preferred_address' => [],
            'current_salary' => $this->faker->numberBetween(1000, 200000),
            'preference_salary' => $this->faker->numberBetween(1000, 250000),
            'date_of_birth' => $this->faker->date(),
            'identity_number' => $this->faker->numberBetween(10000000, 10000000000),
        ];

        $response = $this->post(route('candidate.store'), $data);
        $response->assertStatus(201);
        $response->assertJson(["data" => $data]);
    }

    /** @test */
    public function a_candidate_can_be_updated()
    {
        $this->withoutExceptionHandling();
        $data = [
            'user_id' => $this->faker->numberBetween(1,10),
            'bio' => $this->faker->text(300),
            'preference_description' => $this->faker->text(350),
            'title' => $this->faker->title,
            'preferred_address_id' => $this->faker->numberBetween(1,10),
            'current_salary' => $this->faker->numberBetween(1000, 200000),
            'preference_salary' => $this->faker->numberBetween(1000, 250000),
            'date_of_birth' => $this->faker->date(),
            'identity_number' => $this->faker->numberBetween(10000000, 10000000000),
        ];

        $response = $this->put(route('candidate.update', $this->candidate->id), $data);
        $response->assertJson(["data" => $data]);
    }

    /** @test */
    public function a_candidate_can_be_deleted()
    {
        $this->withoutExceptionHandling();

        $response = $this->delete(route('candidate.destroy', $this->candidate->id));
        $response->assertStatus(204);
    }

    /** @test */
    public function all_candidates_can_be_listed()
    {
        $this->withoutExceptionHandling();
        $response = $this->get(route('candidate.index'));
        $response->assertStatus(200);
    }

    /** @test */
    public function candidate_can_be_viewed()
    {
        $this->withoutExceptionHandling();
        $response = $this->get(route('candidate.show', $this->candidate->id));
        $response->assertJsonCount(1);
    }
}
