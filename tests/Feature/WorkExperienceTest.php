<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\WorkExperience;
use Carbon\Carbon;

class WorkExperienceTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    protected $work_experience;
    protected function setUp(): void
    {
        parent::setUp();

        $this->work_experience = factory(WorkExperience::class)->create();
    }

    /** @test */
    public function is_an_instance_of_work_experience()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(WorkExperience::class, $this->work_experience);
    }

    /** @test */
    public function is_an_instance_of_carbon()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(Carbon::class, $this->work_experience->start_date);
        $this->assertInstanceOf(Carbon::class, $this->work_experience->end_date);
    }
}
