<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\JobHiringProcess;

class JobHiringProcessTest extends TestCase
{
    use RefreshDatabase;
    protected $job_hiring_process;
    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        JobHiringProcess::create();
        $this->job_hiring_process = JobHiringProcess::first();
    }

    /** @test */
    public function is_an_instance_of_job_hiring_process()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(JobHiringProcess::class, $this->job_hiring_process);
    }
}
