<?php

namespace Tests\Feature;

use App\Address;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddressTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    protected $address;

    protected function setUp(): void
    {
        parent::setUp();

        $this->address = factory(Address::class)->create();
    }

    /** @test */
    public function an_address_can_be_created()
    {
        $this->withoutExceptionHandling();
        $data = [
            'user_id' => $this->faker->numberBetween(1, 10),
            'address_line_1' => $this->faker->streetName,
            'address_line_2' => $this->faker->streetAddress,
            'complex' => $this->faker->company,
            'city' => $this->faker->city,
            'province' => $this->faker->text(15),
            'zip_code' => $this->faker->postcode,
            'country' => $this->faker->country
        ];

        $response = $this->post(route('address.store'), $data);
        $response->assertStatus(201);
    }

    /** @test */
    public function an_address_can_be_updated()
    {
        $this->withoutExceptionHandling();
        $data = [
            'user_id' => $this->faker->numberBetween(1, 10),
            'address_line_1' => $this->faker->streetName,
            'address_line_2' => $this->faker->streetAddress,
            'complex' => $this->faker->company,
            'city' => $this->faker->city,
            'province' => $this->faker->text(15),
            'zip_code' => $this->faker->postcode,
            'country' => $this->faker->country
        ];

        $response = $this->put(route('address.update', $this->address->id), $data);
        $response->assertJson(['data' => $data]);
    }

    /** @test */
    public function an_address_can_be_deleted()
    {
        $this->withoutExceptionHandling();

        $response = $this->delete(route('address.destroy', $this->address->id));
        $response->assertStatus(204);
    }

    /** @test */
    public function all_addresses_can_be_listed()
    {
        $this->withoutExceptionHandling();

        factory(Address::class, 9)->create();
        $response = $this->get(route('address.index'));
        $response->assertStatus(200);
    }

    /** @test */
    public function an_address_can_be_viewed()
    {
        $this->withoutExceptionHandling();

        $response = $this->get(route('address.show', $this->address->id));
        $response->assertStatus(200);
    }
}
