<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Role;

class RoleTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    protected $role;
    protected function setUp(): void
    {
        parent::setUp();
        $this->role = factory(Role::class)->create();
    }

    /** @test */
    public function is_an_instance_of_role()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(Role::class, $this->role);
    }

    /** @test */
    public function string_fields_of_role()
    {
        $this->withoutExceptionHandling();
        $this->assertIsString($this->role->name);
        $this->assertIsString($this->role->description);
    }

    /** @test */

    public function a_role_can_be_created()
    {
        $this->withoutExceptionHandling();

        $data = [
            'name' => $this->role->name,
            'description' => $this->role->description
        ];

        $response = $this->post(route('role.store'),$data);
        $response->assertStatus(201);
        $response->assertJson(['data' => $data]);

    }

    /** @test */
    public function a_role_can_be_updated()
    {
        $this->withoutExceptionHandling();
        $data = [
            'name' => 'updated role',
            'description' => 'some description comes here'
        ];
        $response = $this->put(route('role.update', $this->role->id), $data);
        $response->assertJson(['data' => $data]);
    }

    /** @test */
    public function a_role_can_be_deleted()
    {
        $this->withoutExceptionHandling();
        $response = $this->delete(route('role.destroy', $this->role->id));
        $response->assertStatus(204);
    }

    /** @test */
    public function list_all_roles()
    {
        $this->withoutExceptionHandling();
        $response = $this->get(route('role.index'));
        $response->assertStatus(200);
    }
}
