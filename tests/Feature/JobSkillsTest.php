<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\JobSkill;

class JobSkillsTest extends TestCase
{
    use RefreshDatabase;
    protected $job_skill;
    protected function setUp(): void
    {
        parent::setUp();
        $this->job_skill = factory(JobSkill::class)->create();
    }
    /** @test */
    public function is_an_instance_of_job_skill()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(JobSkill::class, $this->job_skill);
    }

    /** @test */
    public function integer_field_of_job_skill()
    {
        $this->withoutExceptionHandling();
        $this->assertIsInt($this->job_skill->client_job_id);
        $this->assertIsInt($this->job_skill->skill_id);
    }
}
