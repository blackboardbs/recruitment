<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    protected $guarded = [];

    public function candidate()
    {
        return $this->belongsTo(Candidate::class, 'candidate_id');
    }

    public function job_application_status()
    {
        return $this->belongsTo(JobApplicationStatus::class, 'job_application_status_id');
    }

    public function job_application_message()
    {
        return $this->belongsTo(JobApplicationMessage::class, 'job_application_message_id');
    }

    public function messages()
    {
        return $this->hasMany(JobApplicationMessage::class);
    }
}
