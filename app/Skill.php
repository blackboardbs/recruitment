<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $guarded = [];

    public function job_skills()
    {
        return $this->hasMany(JobSkill::class);
    }

    public function candidate_skills()
    {
        return $this->hasMany(CandidateSkill::class);
    }
}
