<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobRole extends Model
{
    protected $guarded = [];

    public function recruiter()
    {
        return $this->belongsTo(Recruiter::class,'recruiter_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
}
