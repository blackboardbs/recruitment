<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recruiter extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function recruiter_company()
    {
        return $this->belongsTo(RecruitmentCompany::class, 'recruiter_company_id');
    }
}
