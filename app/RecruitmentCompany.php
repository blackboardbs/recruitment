<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecruitmentCompany extends Model
{
    protected $guarded = [];

    public function client_jobs()
    {
        return $this->hasMany(ClientJob::class);
    }

    public function recruitment_client()
    {
        return $this->hasMany(RecruitmentClient::class);
    }

    public function recruitment_roles()
    {
        return $this->hasMany(RecruiterRole::class);
    }
}
