<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationMessage extends Model
{
    protected $guarded = [];

    public function recruiter()
    {
        return $this->belongsTo(Recruiter::class, 'recruiter_id');
    }

    public function candidate()
    {
        return $this->belongsTo(Candidate::class, 'candidate_id');
    }
}
