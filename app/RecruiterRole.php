<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecruiterRole extends Model
{
    protected $guarded = [];

    public function recruitment_company()
    {
        return $this->belongsTo(RecruitmentCompany::class, 'recruitment_company_id');
    }

    public function recruiter()
    {
        return $this->belongsTo(Recruiter::class, 'recruiter_id');
    }
}
