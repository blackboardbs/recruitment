<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CvController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('cv.buildcv');
    }

    /**
     * Persist a cv
     *
     * @param Request $request
     * @return void
     */
    public function store( Request $request ) {
        // Requires a candidate to be saved.
    }
}
