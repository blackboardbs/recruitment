<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $guarded = [];
    protected $dates = ['date_of_birth'];

    public function setDateOfBirthAttribute($date_of_birth)
    {
        return $this->attributes['date_of_birth'] = Carbon::parse($date_of_birth);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function preferred_address()
    {
        return $this->belongsTo(Address::class, 'preferred_address_id');
    }

    public function work_experiences()
    {
        return $this->hasMany(WorkExperience::class);
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function candidate_skills()
    {
        return $this->hasMany(CandidateSkill::class);
    }

    public function social_links()
    {
        return $this->hasMany(SocialLink::class);
    }

    public function education()
    {
        return $this->hasMany(Education::class);
    }
}
