@extends('layouts.app')
@section('css')
<style>
    .search-container {
        background-image: url('storage/backgrounds/Home.jpg');
        background-size: 110% 110%;
        background-repeat: no-repeat;
        text-align: center;
        width: 80%;
        height: 450px;
        overflow: hidden;
        margin-left: 10%;
    }

    #viewport {
        padding-left: 0px !important;
    }

    .search-heading {
        color: orange;
        margin-top: 180px;
    }

    .container-fluid-ad {
        background-color: #edeef2;
        height: 450px;
        float: left;
        width: 50%;
    }

    .container-fluid-footer {
        background-color: #106ca0;
        text-align: center;
        width: 100%;
        height: 250px;
        overflow: hidden;
        color: white;
    }

</style>
@endsection

@section('content')

        <div class="search-container">
            <h1 class="search-heading">Find the job <span style="color:whitesmoke">you always dreamed of.</span></h1>
            <form action="#" method="POST" role="search" style="display:inline-block; width:800px;">
                {{ csrf_field() }}
                <div class="input-group" style="background-color:gray; border-radius: 5px;">
                    <input style="width:150px; margin:10px" type="text" class="form-control" name="job" placeholder="Search for job title or keyword">
                    <input style="margin:10px" type="text" class="form-control" name="location" placeholder="Location">
                    <button style="margin:10px" type="submit" class="btn btn-warning">SEARCH</button>    
                </div>
            </form>
        </div>

        <div class="row">
            <div class="container-fluid-ad">
                <img style="height:100%; width:100%;" src="{{url('storage/images/holdingresume5.jpg')}}" alt="">
            </div>
            <div class="container-fluid-ad">
                 <h3 style="margin-top:150px; margin-left:80px">Build your resume for <strong>FREE</strong> and find your dream job.</h3>
                <button style="margin-top:30px; margin-left:80px" type="button" class="btn btn-primary"><small>BUILD YOUR RESUME NOW</small></button>
            </div>
        </div>

        <div class="row">
            <div class="container-fluid-ad" style="background-color:#3d3d3db3; text-align:right">
                <i class="fas fa-globe-africa fa-10x" style="float:right; margin-top:100px; margin-right:100px"></i>
                <h2 style="margin-top:150px"><strong style="margin-right:30px">I AM A JOBSEEKER</strong></h2>
                <small style="margin-right:30px">Create your professional resume with a online resume builder and start applying for the best jobs.</small>
            </div>
            <div class="container-fluid-ad" style="background-color:#13b5eab3; text-align:left">
                <i class="fas fa-building fa-10x" style="float:left; margin-top:100px; margin-left:100px"></i>
                <h2 style="margin-top:150px;"><strong style="margin-left:30px">I AM AN EMPLOYER</strong></h2>
                <small style="margin-left:30px">Job posting and online resume database search service that helps you find the best talent.</small>
            </div>
        </div>

        <div class="row" style="bottom:0;">
            <div class="container-fluid-footer">
                <div class="row">
                    <div class="col-md-3">
                        <img style="margin-top:80px;" src="{{ url('storage/logos/CNLogo2.png') }}" alt="">
                    </div>
                    <div class="col-md-3" style="text-align: left">
                        <h4 style="margin-top:80px;"><strong>Our Location</strong></h4>
                        <h2 style="font-weight:lighter">Menlo Park,</h2>
                        <h2 style="font-weight:lighter">Pretoria</h2>
                    </div>
                    <div class="col-md-3" style="text-align: left">
                        <h4 style="margin-top:80px;"><strong>Contact</strong></h4>
                        <h3 style="font-weight:lighter">(082) 540 8464</h3>
                        <h3 style="font-weight:lighter">info@careernetwork.co.za</h3>
                    </div>
                    <div class="col-md-3" style="text-align: left">
                            <i class="fab fa-facebook-f" style="margin-top:100px;"></i>
                            <i class="fab fa-twitter"></i>
                            <i class="fab fa-linkedin-in"></i>
                    </div>
                </div>
            </div>
        </div>

@endsection