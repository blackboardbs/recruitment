<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CareerNetwork</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link type="text/css" rel="stylesheet" href="{{ mix('css/app.css') }}">
    

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
    
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

    </style>

    <style>
    
      body {
        overflow-x: hidden;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
      }

      /* Toggle Styles */

      
      #viewport {
        padding-left: 250px;
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
      }

      #content {
        width: 100%;
        position: relative;
        margin-right: 0;
      }

      /* Sidebar Styles */

      #sidebar {
        z-index: 1;
        position: fixed;
        left: 250px;
        width: 250px;
        height: 100%;
        margin-left: -250px;
        overflow-y: auto;
        background: #37474F;
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
        top: 0;
      }

      #sidebar header {
        background-color: #263238;
        font-size: 20px;
        line-height: 52px;
        text-align: center;
      }

      #sidebar header a {
        color: #fff;
        display: block;
        text-decoration: none;
      }

      #sidebar header a:hover {
        color: #fff;
      }

      #sidebar .nav{
        
      }

      #sidebar .nav a{
        background: none;
        border-bottom: 1px solid #455A64;
        color: #CFD8DC;
        font-size: 14px;
        padding: 16px 24px;
      }

      #sidebar .nav a:hover{
        background: none;
        color: #ECEFF1;
      }

      #sidebar .nav a i{
        margin-right: 16px;
      }
    
    </style>

    @yield('css')
</head>
<body>

  <div id="viewport">  
  <!--Navbar-->
  <nav class="navbar navbar-expand-lg navbar-dark navbar-fixed-top" style="background-color: #25c3d8;">
  <a style="margin-left:5%" class="navbar-brand" href="#"><img src="{{ url('storage/logos/CNLogo.png') }}" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul style="margin-right:10%" class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" style="margin-right:10px" href="#">JOB SEARCH</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" style="margin-right:10px" href="#">COURSES SEEK</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" style="margin-right:10px" href="#">CAREER RESOURCES</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" style="margin-right:10px" href="#">CANDIDATE SEARCH</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" style="margin-right:10px" href="#">RECRUITER SIGN IN</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" style="margin-right:10px" href="#">SIGN IN</a>
        </li>
        <li class="nav-item">
          <button type="button" class="btn btn-info" href="#">BUILD YOUR CV</button>
        </li>
      </ul>
    </div>
  </nav>
  <!--/navbar-->

  @auth
  <!--Menu-->
      <div id="sidebar">
        <header style="margin-bottom:20px; padding-bottom:20px; padding-top:20px">
          <a href="#">Welcome $user</a>
        </header>
        <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" style="margin-right:10px" href="#">My Career Profile</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" style="margin-right:10px" href="#">My Applications</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" style="margin-right:10px" href="#">CV Builder</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" style="margin-right:10px" href="#">Job Search</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" style="margin-right:10px" href="#">Career Academy</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" style="margin-right:40px" href="#">Settings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" style="margin-right:10px" href="#">Sign Out</a>
            </li>
        </ul>
      </div>
      @endauth

      

    </div>
  <!--/menu-->
        
  <!-- Content -->
  
      @yield('content')
  

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
      $(document).ready(function ()
      {
        $('#sidebarCollapse').on('click', function () 
        {
          $('#sidebar').toggleClass('active');
        });
      });
    </script>
    @yield('scripts')
</body>
</html>