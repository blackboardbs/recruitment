<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecruiterRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recruiter_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('recruitment_company_id');
            $table->unsignedInteger('recruiter_id');
            $table->timestamps();
            $table->index('recruitment_company_id');
            $table->index('recruiter_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recruiter_roles');
    }
}
