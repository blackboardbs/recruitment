<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use App\Role;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone_number' => $faker->e164PhoneNumber,
        'profile_photo' => $faker->imageUrl(150, 150, 'people'),
        'role_id' => $faker->numberBetween(1, Role::count()),
        //'address_id' => $faker->numberBetween(1, Address::count()),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now()->subHours(1)->toDateTime(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
