<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ExperienceLevel;
use Faker\Generator as Faker;

$factory->define(ExperienceLevel::class, function (Faker $faker) {
    return [
        'name' => $faker->text(12)
    ];
});
