<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Attachment;
use App\Candidate;
use App\ClientJob;
use App\FileType;
use Faker\Generator as Faker;

$factory->define(Attachment::class, function (Faker $faker) {
    return [
        'client_job_id' => $faker->numberBetween(1, ClientJob::count()),
        'candidate_id' => $faker->numberBetween(1, Candidate::count()),
        'file_type_id' => $faker->numberBetween(1, FileType::count()),
        'file_name' => $faker->text(25),
    ];
});
