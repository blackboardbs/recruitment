<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\JobApplicationStatus;
use Faker\Generator as Faker;

$factory->define(JobApplicationStatus::class, function (Faker $faker) {
    return [
        'name' => $faker->jobTitle,
    ];
});
