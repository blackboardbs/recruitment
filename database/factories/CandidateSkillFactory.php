<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Candidate;
use App\CandidateSkill;
use App\Skill;
use Faker\Generator as Faker;

$factory->define(CandidateSkill::class, function (Faker $faker) {
    return [
        'candidate_id' => $faker->numberBetween(1, Candidate::count()),
        'skill_id' => $faker->numberBetween(1, Skill::count()),
    ];
});
