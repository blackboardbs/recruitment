<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Candidate;
use App\JobApplication;
use App\JobApplicationMessage;
use App\Recruiter;
use Faker\Generator as Faker;

$factory->define(JobApplicationMessage::class, function (Faker $faker) {
    return [
        'recruiter_id' => $faker->numberBetween(1, Recruiter::count()),
        'candidate_id' => $faker->numberBetween(1, Candidate::count()),
        'message' => $faker->text,
        'job_application_id' => $faker->numberBetween(1, JobApplication::count())
    ];
});
