<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Candidate;
use App\Education;
use App\Institution;
use App\QualificationType;
use Faker\Generator as Faker;

$factory->define(Education::class, function (Faker $faker) {
    return [
        'qualification' => $faker->jobTitle,
        'degree' => $faker->jobTitle,
        'candidate_id' => $faker->numberBetween(1, Candidate::count()),
        'completion_status' => $faker->boolean,
        'qualification_type_id' => $faker->numberBetween(1, QualificationType::count()),
        'institution_id' => $faker->numberBetween(1, Institution::count()),
        'start_date' => $faker->dateTime('-6 years'),
        'end_date' => $faker->dateTime('-3 years'),
        'type' => $faker->jobTitle,
        'current' => $faker->boolean,
        'description' => $faker->text
    ];
});
