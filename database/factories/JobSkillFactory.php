<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ClientJob;
use App\JobSkill;
use App\Skill;
use Faker\Generator as Faker;

$factory->define(JobSkill::class, function (Faker $faker) {
    return [
        'client_job_id' => $faker->numberBetween(1, ClientJob::count()),
        'skill_id' => $faker->numberBetween(1, Skill::count()),
        'experience_years' => $faker->numberBetween(1, 40),
    ];
});
