<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\JobRole;
use App\Recruiter;
use App\Role;
use Faker\Generator as Faker;

$factory->define(JobRole::class, function (Faker $faker) {
    return [
        'recruiter_id' => $faker->numberBetween(1, Recruiter::count()),
        'role_id' => $faker->numberBetween(1, Role::count()),
    ];
});
