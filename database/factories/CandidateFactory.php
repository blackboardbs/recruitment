<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use App\Candidate;
use App\User;
use Faker\Generator as Faker;

$factory->define(Candidate::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, User::count()),
        'bio' => $faker->text(150),
        'preference_description' => $faker->text(150),
        'title' => $faker->title,
        'preferred_address_id' => $faker->numberBetween(1, Address::count()),
        'current_salary' => $faker->numberBetween(1000, 1000000),
        'preference_salary' => $faker->numberBetween(1000, 1000000),
        'date_of_birth' => now()->subYears(18)->toDayDateTimeString(),
        'identity_number' => $faker->numberBetween(1000000000),
    ];
});
