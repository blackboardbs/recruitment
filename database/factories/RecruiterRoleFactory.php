<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Recruiter;
use App\RecruiterRole;
use App\RecruitmentCompany;
use Faker\Generator as Faker;

$factory->define(RecruiterRole::class, function (Faker $faker) {
    return [
        'recruitment_company_id' => $faker->numberBetween(1, RecruitmentCompany::count()),
        'recruiter_id' => $faker->numberBetween(1, Recruiter::count()),
    ];
});
