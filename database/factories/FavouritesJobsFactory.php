<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Candidate;
use App\ClientJob;
use App\FavouritesJobs;
use Faker\Generator as Faker;

$factory->define(FavouritesJobs::class, function (Faker $faker) {
    return [
        'client_job_id' => $faker->numberBetween(1, ClientJob::count()),
        'candidate_id' => $faker->numberBetween(1, Candidate::count()),
    ];
});
