<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CompanyWorkExperience;
use Faker\Generator as Faker;

$factory->define(CompanyWorkExperience::class, function (Faker $faker) {
    return [
        'description' => $faker->text,
        'city' => $faker->city,
        'name' => $faker->jobTitle,
    ];
});
