<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Role;
use App\UserRole;
use Faker\Generator as Faker;

$factory->define(UserRole::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, UserRole::count()),
        'role_id' => $faker->numberBetween(1, Role::count()),
    ];
});
